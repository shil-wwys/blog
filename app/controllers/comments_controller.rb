class CommentsController < ApplicationController
  before_action :authenticate_user!

  def create
    @article = Article.find params[:article_id]
    @comment = @article.comments.build(comment_params)   
    @comment.user = current_user

    if @comment.save
      flash[:success] = 'Comment is created successfully'
    else
      flash[:error] = @comment.errors.full_message
    end
    redirect_to @article
  end

  def comment_params
   params.require(:comment).permit(:body)
  end
end
