class Article < ApplicationRecord
  belongs_to :user, validate: true
  has_many :comments

  validates :title, :text, presence: true
end
