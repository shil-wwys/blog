class Comment < ApplicationRecord
  belongs_to :user, validate: true
  belongs_to :article, validate: true
 
  validates :body, presence: true

end
