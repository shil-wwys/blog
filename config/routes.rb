Rails.application.routes.draw do
  get 'comments/create'
  devise_for :users
  root 'home#index'
  
  resources :articles do
    resources :comments
  end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
